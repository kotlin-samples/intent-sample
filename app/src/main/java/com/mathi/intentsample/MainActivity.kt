package com.mathi.intentsample

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var keyCodes: KeyCodes? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        keyCodes = KeyCodes()

        next_activity.setOnClickListener { nextActivity() }

        next_activity_with_value.setOnClickListener { nextActivityWithValues() }
    }

    private fun nextActivity() {
        startActivity(Intent(this, NextActivity::class.java))
    }

    private fun nextActivityWithValues() {
        val nextIntent = Intent(this, NextActivity::class.java)
        nextIntent.putExtra(keyCodes!!.name, "Mathi")
        startActivity(nextIntent)
    }
}
