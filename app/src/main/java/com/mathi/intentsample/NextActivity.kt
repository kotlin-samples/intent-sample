package com.mathi.intentsample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast

class NextActivity : AppCompatActivity() {
    var keyCodes: KeyCodes? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_next)
        keyCodes = KeyCodes()

        if (intent.hasExtra(keyCodes!!.name)) {
            Toast.makeText(this, getString(R.string.hi) + " " + intent.getStringExtra(keyCodes!!.name), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, getString(R.string.no_name), Toast.LENGTH_SHORT).show()
        }
    }
}
